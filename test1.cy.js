describe('empty spec', () => {
  it('passes', () => {
    cy.visit('https://app.privy.id/')

    cy.visit('https://oauth.privy.id')
    cy.title().should('equal', 'PrivyID Oauth')
    cy.get('#__BVID__4').type('MIE5839')

    cy.contains('button', 'CONTINUE').as('loginBtn')
    cy.get('@loginBtn').click()

    cy.get('#__BVID__6').type('Mbakalan1905')
    cy.get('@loginBtn').click()

    cy.wait(4000)
    cy.contains('button', 'Upload Document').as('btnUploadDocs')
    cy.get('@btnUploadDocs').click()
    cy.wait(3000)

    cy.contains('Self Sign').as('btnSelfSign')
    cy.get('@btnSelfSign').click()
    cy.wait(3000)
    cy.go('back')

    cy.contains('button', 'Upload Document').as('btnUploadDocs')
    cy.get('@btnUploadDocs').click()
    cy.wait(3000)

    cy.contains('Sign & Request').as('btnSignReq')
    cy.get('@btnSignReq').click()
    cy.wait(3000)
    cy.go('back')

    cy.contains('Change My Signature Image').as('btnChangeImg')
    cy.get('@btnChangeImg').click()
    cy.wait(3000)
    cy.go('back')


  })
})